defmodule Jirelix do
  use HTTPoison.Base
  alias Jirelix.Client

  @default_header [{"Accept", "application/json"}, {"Content-Type", "application/json"}]

  defp url(_client = %Client{endpoint: endpoint}, path) do
    endpoint <> path
  end

  defp authorization_header(%Client{username: user, password: password}, headers) do
    userpass = "#{user}:#{password}"
    headers ++ [{"Authorization", "Basic #{:base64.encode(userpass)}"}]
  end

  defp process_response(%HTTPoison.Response{status_code: status_code, body: body}) do
    cond do
      status_code == 401 -> {:error, :unauthorized}
      Enum.member?(400..499, status_code) -> {:error, JSX.decode!(body)}
      Enum.member?(200..299, status_code) -> {:ok, JSX.decode!(body)}
    end
  end

  def get(path, client = %Client{}, headers \\ [], _options \\ []) do
    url = url(client, path)
    all_headers = authorization_header(client, @default_header ++ headers)
    get!(url, all_headers, [ssl: [{:versions, [:'tlsv1.2']}]]) |> process_response
  end
end
