defmodule Jirelix.Search do
  import Jirelix
  alias Jirelix.Client

  require Logger

  def project_issues(project_keys, issue_keys, %Client{} = client) do
    jql = generate_jql(project_keys, issue_keys)
    encoded = URI.encode(jql)
    get("search?jql=#{encoded}", client)
  end

  def generate_jql(projects, issue_keys) do
    projects
    |> format_projects
    |> basic_jql(format_issues(issue_keys))
  end

  def format_issues(issues) when is_binary(issues) do
    issues
  end
  def format_issues(issues) when is_list(issues) do
    Enum.join(issues, ", ")
  end

  def format_projects(projects) when is_binary(projects) do
    projects
  end
  def format_projects(projects) when is_list(projects) do
    Enum.join(projects, ", ")
  end

  def basic_jql(project, issue_keys) do
    "project in (#{project}) and issuekey in (#{issue_keys})"
  end
end
