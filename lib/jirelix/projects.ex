defmodule Jirelix.Projects do
  import Jirelix
  alias Jirelix.Client

  def all(client = %Client{}) do
    get("project", client)
  end

  def find(project_id, client = %Client{}) do
    get("project/#{project_id}", client)
  end
end
