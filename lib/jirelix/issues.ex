defmodule Jirelix.Issues do
  import Jirelix
  alias Jirelix.Client

  def find(issue_id, client = %Client{}) do
    get("issue/#{issue_id}", client)
  end
end
