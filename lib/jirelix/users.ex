defmodule Jirelix.Users do
  import Jirelix
  alias Jirelix.Client

  def myself(client = %Client{}) do
    get("myself", client)
  end
end
