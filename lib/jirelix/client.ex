defmodule Jirelix.Client do
  defstruct username: nil, password: nil, endpoint: nil

  def new(username, password, endpoint) do
    endpoint = if String.ends_with?(endpoint, "/") do
      endpoint
    else
      endpoint <> "/"
    end
    %__MODULE__{username: username, password: password, endpoint: endpoint}
  end
end
